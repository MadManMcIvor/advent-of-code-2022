file = open('inputs.txt', 'r')
read = file.readlines()

result_list = []

temp_total = 0

for item in read:
    if item == "\n":
        result_list.append(temp_total)
        temp_total = 0
        continue
    num = item[0:-1]
    temp_total += int(num)

results_sorted = sorted(result_list)
print(sum(results_sorted[-3:]))
