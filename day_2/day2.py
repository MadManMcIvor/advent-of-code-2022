#Rock, Paper, Scissors Tourney!

#Steps:
#import inputs
#Process them. Maybe make them a list of tuples? Eh probably no need can just make a list
#Iterate through and make a mesh of conditionals, add score to running tally

file = open("day2_inputs.txt", "r")
read = file.readlines()

opp_list = []
my_list = []
total = 0


#Part 1
# for match in read:
#     me = match[2]
#     opp = match[0]
#     if me == "X":
#         total += 1
#     elif me == "Y":
#         total += 2
#     else: 
#         total += 3
#     if me == "X":
#         if opp == "A":
#             total += 3
#         elif opp == "B":
#             total += 0
#         else: 
#             total += 6
#     if me == "Y":
#         if opp == "A":
#             total += 6
#         elif opp == "B":
#             total += 3
#         else: 
#             total += 0
#     if me == "Z":
#         if opp == "A":
#             total += 0
#         elif opp == "B":
#             total += 6
#         else: 
#             total += 3


#Part 2
for match in read:
    me = match[2]
    opp = match[0]
    if me == "X":
        total += 0
    elif me == "Y":
        total += 3
    else: 
        total += 6
    
    if me == "X": #I need to lose
        if opp == "A": #Rock
            total += 3 #Scissors
        elif opp == "B": #Paper
            total += 1 #Rock
        else:  #Scissors
            total += 2 #Paper
   
    if me == "Y":  #I need to draw
        if opp == "A":
            total += 1
        elif opp == "B":
            total += 2
        else: 
            total += 3
    
    if me == "Z": #I need to win
        if opp == "A": #Rock
            total += 2 #Paper
        elif opp == "B": #Paper
            total += 3 #Scissors
        else: #Scissors
            total += 1 #Rock

print(total)



