#--- Day 3: Rucksack Reorganization ---

#import inputs
#loop through inputs
#add first half of the item into a dictionary
#check to see if the second half of the dictionary is in the first dictionary.
#whatever one is in, convert to a number and then add it to a running total


#Part 1
# file = open("day3_inputs.txt", "r")
# read = file.readlines()

# total = 0

# for rucksack in read:
#     #this cuts off the '\n' at the end of each item except for the last item which doesn't have it
#     if rucksack[-1] == "\n":
#         rucksack = rucksack[0:-1]
#     first = rucksack[:len(rucksack)//2]
#     second = rucksack[len(rucksack)//2:]
#     for char in second:
#         if char in first:
#             if char.islower():
#                 total += ord(char) - 96
#                 break
#             else:
#                 total += ord(char) - 38
#                 break
# print(total)


#Part 2

file = open("day3_inputs.txt", "r")
read = file.readlines()

score = 0
first = ''
second = ''

count = 0

for rucksack in read:
    if rucksack[-1] == "\n":
        rucksack = rucksack[0:-1]
    count += 1
    if count == 1:
        first = rucksack
    elif count == 2:
        second = rucksack
    else:
        for char in rucksack:
            if char in first:
                if char in second:
                    if char.islower():
                        score += ord(char) - 96
                        break
                    else:
                        score += ord(char) - 38
                        break
        count = 0

print(score)
