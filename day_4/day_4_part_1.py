#--- Day 4: Camp Cleanup ---

#steps:
# process data
# initalize counter = 0
# loop through data
# if left pair is fully contained by right, add to conunter
# if right pair is fully contained by left, add to counter
# return counter

import re #importing this so I can split using more than one delimiter

counter = 0

with open("day4_inputs.txt", "r") as f:
    inputs = f.readlines()
    for pair in inputs:
        if pair[-1] == "\n":
            pair = pair[0:-1]
            split_pair = re.split('[,-]', pair)
            if int(split_pair[0]) >= int(split_pair[2]) and int(split_pair[1]) <= int(split_pair[3]):
                counter += 1
                continue
            elif int(split_pair[2]) >= int(split_pair[0]) and int(split_pair[3]) <= int(split_pair[1]):
                counter += 1
                print(split_pair, counter)
                continue
print(counter)