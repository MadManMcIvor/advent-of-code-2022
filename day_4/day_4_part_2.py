#--- Day 4: Camp Cleanup ---

import re 

counter = 0

with open("day4_inputs.txt", "r") as f:
    inputs = f.readlines()
    for pair in inputs:
        if pair[-1] == "\n":
            pair = pair[0:-1]
        split_pair = re.split('[,-]', pair)
        range1 = range(int(split_pair[0]), int(split_pair[1])+1)
        range2 = range(int(split_pair[2]), int(split_pair[3])+1)
        if int(split_pair[0]) in range2 or int(split_pair[1]) in range2:
            counter += 1
            continue
        elif int(split_pair[2]) in range1 or int(split_pair[3]) in range1:
            counter += 1
            continue
        

#I spent so long trying to figure out why I was getting the wrong answer. It turns out I was indented 
#so it was missing the last item, so my count kept being off by 1. I thought it was due to the logic
#so it took me forever to find it after looking at someone else's code.

#Broken Code:
# with open("day4_inputs.txt", "r") as f:
#     inputs = f.readlines()
#     for pair in inputs:
#         if pair[-1] == "\n":
#             pair = pair[0:-1]
#             split_pair = re.split('[,-]', pair)
#             range1 = range(int(split_pair[0]), int(split_pair[1])+1)
#             range2 = range(int(split_pair[2]), int(split_pair[3])+1)
#             if int(split_pair[0]) in range2 or int(split_pair[1]) in range2:
#                 counter += 1
#                 continue
#             elif int(split_pair[2]) in range1 or int(split_pair[3]) in range1:
#                 counter += 1
#                 continue
            



#Another way to solve it
        
# with open("day4_inputs.txt", "r") as f:
#     inputs = f.readlines()
#     for pair in inputs:
#         if pair[-1] == "\n":
#             pair = pair[0:-1]
#         split_pair = re.split('[,-]', pair)
#         if int(split_pair[0]) > int(split_pair[3]):
#             continue
#         elif int(split_pair[1]) < int(split_pair[2]):
#             continue
#         else:
#             counter += 1

print(counter)