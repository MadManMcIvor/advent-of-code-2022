stacks = {
    "1": ["F", "H", "B", "V", "R", "Q", "D", "P"],
    "2": ["L", "D", "Z", "Q", "W", "V"],
    "3": ["H", "L", "Z", "Q", "G", "R", "P", "C"],
    "4": ["R", "D", "H", "F", "J", "V", "B"],
    "5": ["Z", "W", "L", "C"],
    "6": ["J", "R", "P", "N", "T", "G", "V", "M"],
    "7": ["J", "R", "L", "V", "M", "B", "S"],
    "8": ["D", "P", "J"],
    "9": ["D", "C", "N", "W", "V"]
}


with open("day5_inputs.txt", "r") as f:
    moves = f.readlines()
    for move in moves:
        move = move.strip()
        move_list = move.split(' ')
        how_many = int(move_list[1])
        from_where = move_list[3]
        to_where = move_list[5]
        temp = stacks[from_where][-how_many:]
        stacks[to_where] += temp
        stacks[from_where] = stacks[from_where][0:-how_many]


result = ''
for key, value in stacks.items():
    if len(value):
        result += value[-1]

print(result)