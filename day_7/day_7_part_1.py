
class TreeNode:
    def __init__(self, name, parent=None):
        self.name = name
        self.sum = 0
        self.children = []
        self.parent = parent

    def add_child(self, node):
        self.children.append(node)

    def find_sum(self):
        for child in self.children:
            if child == None:
                return
            elif type(child) == int:
                self.sum += child
            else:
                child.find_sum()
                self.sum += child.sum
           

root = TreeNode("/")

with open('day7_inputs.txt', 'r') as f:
    instructions = f.readlines()
    present_dir = root
    for line in instructions:
        line = line.strip()
        if line == "$ cd /":
            present_dir = root
            continue
        elif line == "$ cd ..":
            if present_dir.parent == None:
                continue
            present_dir = present_dir.parent
        elif line == "$ ls":
            continue
        elif line[0].isnumeric():
            number = int(line.split(" ")[0])
            if number not in present_dir.children:
                present_dir.add_child(number)
        elif line[0] == "d":
            name = line.split(' ')[1]
            node = TreeNode(name, present_dir)
            switch = False
            for child in present_dir.children:
                if type(child) == int:
                    continue
                else:
                    if node.name == child.name:
                        switch == True
            if switch == False:            
                present_dir.add_child(node)
        else:
            name = line.split()[2]
            for item in present_dir.children:
                if type(item)==int:
                    continue
                elif item.name == name:
                    present_dir = item

sums_list = []

def track_sum(node):
    if type(node) == int:
        return
    sums_list.append(node.sum)
    if node.children == []:
        return
    else:
        for child in node.children:
            track_sum(child)



root.find_sum()
track_sum(root)

print(sums_list)

result = 0
for item in sums_list:
    if item <= 100000:
        result += item

print(result)