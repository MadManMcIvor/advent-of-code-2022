# Day 7
Holy hell this one was a lot harder. My method worked but I had to iron out a few bugs in the code to finally get it to work. Took a while to figure out what the errors were. 

# Day 6 
This one was the easiest so far, largely because there was less work to be done with the inputs since it could just be copied and pasted directly. I spent a while thinking of my approach since I wanted one that'd have as small a time complexity as I could manage without wasting too much time thinking about the problem. I went with a sliding window and then used the set method to weed out any duplicates. 

# Day 5
The most part of this project was spend creating the starting dictionary. Though I could have probably done it another way. I was wondering if I should do a stack implementation, but the dict seemed to work just fine. My main problem was I misread the problem at first and happened to make the execution the way it wanted for part 2. Oh well, it made doing part 2 super simple. 

I think I may want to try going through and implementing this problems in typescript, so I can get the hang of it.